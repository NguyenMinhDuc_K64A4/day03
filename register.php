<!DOCTYPE html> 
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Login</title>
<body>



    <fieldset style='width: 500px; height: 500px; border:#ADD8E6 solid'>
    <?php
    // define variables and set to empty values
        $name = $gender = $industryCode = $birthOfDate = $address= "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if(empty(inputHandling($_POST["name"]))){
                echo "<div style='color: red;'>Hãy nhập tên</div>";
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính</div>";
            }
            if(empty(inputHandling($_POST["industryCode"]))){
                echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
            }
    
        }

        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }
    ?>
    <form style='margin: 20px 50px 0 35px' method="post">
        <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #3d9dd4; 
                vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Họ và tên</label>
                </td>
                <td width = 30% >
                    <input type='text' name = "name" style = 'line-height: 32px; border-color:#ADD8E6'>
                </td>
            </tr>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #3d9dd4; vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Giới tính</label>
                </td>
                <td width = 30% >
                <?php
                    $genderArr=array("Nam","Nữ");
                    for($x = 0; $x < count($genderArr); $x++){
                        echo"
                            <label class='container'>
                                <input type='radio' value=".$genderArr[$x]." name='gender'>"
                                .$genderArr[$x]. 
                            "</label>";
                    }
                ?>  

                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #3d9dd4; vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Phân Khoa</label>
                </td>
                <td height = '40px'>
                    <select name='industryCode' style = 'border-color:#ADD8E6;height: 100%;width: 80%;'>
                        <?php
                            $industryCodeArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($industryCodeArr as $x=>$x_value){
                                echo"<option>".$x_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
        </table>
        <button style='background-color: #49be25; border-radius: 10px; 
        width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
    </form>

</fieldset>
</body>
</html>